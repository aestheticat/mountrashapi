using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MountrashApi.Models
{
    public class CatalogType
    {
        [Key]
        public long CatalogId { get; set; }
        public string Name { get; set; }
        public DateTime LastUpdated { get; set; }
        public List<CatalogSubType> CatalogSubTypes { get; set; }
    }
}