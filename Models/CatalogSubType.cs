using System;
using System.ComponentModel.DataAnnotations;

namespace MountrashApi.Models
{
    public class CatalogSubType
    {
        
        [Key]
        public long SubId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime LastUpdated { get; set; }   
        public long CatalogId { get; set; }
        public CatalogType CatalogType { get; set; }
    }
}