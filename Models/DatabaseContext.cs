using Microsoft.EntityFrameworkCore;

namespace MountrashApi.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CatalogSubType>()
                .HasOne(s => s.CatalogType)
                .WithMany(c => c.CatalogSubTypes)
                .HasForeignKey(s => s.CatalogId);
        }

        public DbSet<CatalogType> CatalogTypes { get; set; }
        public DbSet<CatalogSubType> CatalogSubTypes { get; set; }
    }
}