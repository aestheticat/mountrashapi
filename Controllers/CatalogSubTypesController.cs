using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountrashApi.Models;

namespace MountrashApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogSubTypesController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public CatalogSubTypesController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/CatalogSubTypes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CatalogSubType>>> GetCatalogSubTypes()
        {
            return await _context.CatalogSubTypes.ToListAsync();
        }

        // GET: api/CatalogSubTypes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CatalogSubType>> GetCatalogSubType(long id)
        {
            var catalogSubType = await _context.CatalogSubTypes.FindAsync(id);

            if (catalogSubType == null)
            {
                return NotFound();
            }

            return catalogSubType;
        }

        // PUT: api/CatalogSubTypes/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCatalogSubType(long id, CatalogSubType catalogSubType)
        {
            if (id != catalogSubType.SubId)
            {
                return BadRequest();
            }

            _context.Entry(catalogSubType).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CatalogSubTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CatalogSubTypes
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CatalogSubType>> PostCatalogSubType(CatalogSubType catalogSubType)
        {
            _context.CatalogSubTypes.Add(catalogSubType);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetCatalogSubTypes), new { id = catalogSubType.SubId }, catalogSubType);
        }

        // DELETE: api/CatalogSubTypes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCatalogSubType(long id)
        {
            var catalogSubType = await _context.CatalogSubTypes.FindAsync(id);
            if (catalogSubType == null)
            {
                return NotFound();
            }

            _context.CatalogSubTypes.Remove(catalogSubType);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CatalogSubTypeExists(long id)
        {
            return _context.CatalogSubTypes.Any(e => e.SubId == id);
        }
    }
}
