using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountrashApi.Models;

namespace MountrashApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogTypesController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public CatalogTypesController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/CatalogTypes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CatalogType>>> GetCatalogTypes()
        {
            return await _context.CatalogTypes.ToListAsync();
        }

        // GET: api/CatalogTypes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CatalogType>> GetCatalogType(long id)
        {
            var catalogType = await _context.CatalogTypes.FindAsync(id);

            if (catalogType == null)
            {
                return NotFound();
            }

            return catalogType;
        }

        // PUT: api/CatalogTypes/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCatalogType(long id, CatalogType catalogType)
        {
            if (id != catalogType.CatalogId)
            {
                return BadRequest();
            }

            _context.Entry(catalogType).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CatalogTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CatalogTypes
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CatalogType>> PostCatalogType(CatalogType catalogType)
        {
            _context.CatalogTypes.Add(catalogType);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetCatalogType),  new { id = catalogType.CatalogId }, catalogType);
        }

        // DELETE: api/CatalogTypes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCatalogType(long id)
        {
            var catalogType = await _context.CatalogTypes.FindAsync(id);
            if (catalogType == null)
            {
                return NotFound();
            }

            _context.CatalogTypes.Remove(catalogType);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CatalogTypeExists(long id)
        {
            return _context.CatalogTypes.Any(e => e.CatalogId == id);
        }
    }
}
